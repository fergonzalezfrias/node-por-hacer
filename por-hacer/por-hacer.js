const fs = require('fs');

let listadoPorHaer = [];

const guardarDb = () => {

    let data = JSON.stringify(listadoPorHaer);
    fs.writeFile('db/data.json', data, (err) => {
        if (err)
            throw new Error('No se pudo guardar la información');
        else
            console.log('Información guardada correctamente');
    });

};

const cargarDb = () => {

    try {
        listadoPorHaer = require('../db/data.json');
    } catch (err) {
        listadoPorHaer = [];
    }
};

const getListado = () => {
    cargarDb();
    return listadoPorHaer;
};

const actualizar = (descripcion, completado = true) => {

    cargarDb();
    let index = listadoPorHaer.findIndex((tarea) => tarea.descripcion === descripcion);
    if (index >= 0) {
        listadoPorHaer[index].completado = completado;
        guardarDb();
        console.log(`La tarea cn la descripción: '${descripcion}' se ha actualizado con éxito.`)
    } else {
        console.log(`La tarea con la descripción: '${descripcion}' no existe.`);
    }

};

const borrar = (descripcion) => {
    cargarDb();
    /* let index = listadoPorHaer.findIndex((tarea) => tarea.descripcion === descripcion);
    if (index >= 0) {
        listadoPorHaer.splice(index, 1);
        guardarDb();
        console.log(`La tarea con la descripción: '${descripcion}' se ha eliminado con éxito.`)
    } else {
        console.log(`La tarea con la descripción: '${descripcion}' no existe.`);
    } */
    let nuevoListado = listadoPorHaer.filter((tarea) => {
        return tarea.descripcion !== descripcion;
    });

    if (nuevoListado.length == listadoPorHaer.length) {
        console.log(`"La tarea con la descripción '${descripcion}' no existe`);
    } else {
        listadoPorHaer = nuevoListado;
        guardarDb();
        console.log(`La tarea con la descripción '${descripcion}' se borró correctamente`);
    }
};

const crear = (descripcion) => {

    cargarDb();

    let porHacer = {
        descripcion,
        completado: false
    };

    listadoPorHaer.push(porHacer);

    guardarDb();

    return porHacer;
};

module.exports = {
    crear,
    getListado,
    actualizar,
    borrar
};
//const { argv } = require('yargs');
const argv = require('./config/yargs').argv;
const colors = require('colors');
const { crear, getListado, actualizar, borrar } = require('./por-hacer/por-hacer');

let comando = argv._[0];

switch (comando) {
    case 'crear':
        let tarea = crear(argv.descripcion);
        console.log(tarea);
        break;
    case 'listar':
        let tareas = getListado();
        for (let tarea of tareas) {
            console.log('===================POR HACER====================='.green);
            console.log(`Descripción: ${tarea.descripcion}`);
            console.log(`Completado:  ${tarea.completado}`);
            console.log('================================================='.green);
        }
        break;
    case 'actualizar':
        actualizar(argv.descripcion, argv.completado);
        break;
    case 'borrar':
        borrar(argv.descripcion);
        break;
    default:
        console.log('El comando no se RECONOCE');
        break;
}